package com.guosen.zebra.gateway.route.parser;

import com.guosen.zebra.gateway.route.model.RouteConfig;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import com.guosen.zebra.gateway.route.model.SubUrlMethodMapping;
import mockit.Expectations;
import mockit.Mocked;
import mockit.Tested;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class ConcreteUrlRouteInfoParserTest {

    @Tested
    private ConcreteUrlRouteInfoParser concreteUrlRouteInfoParser;

    @Mocked
    private SubUrlMethodMappingParser subUrlMethodMappingParser;

    @Test
    public void testNoConfig() {
        new Expectations() {
            {
                SubUrlMethodMappingParser.parse(anyString);
                result = null;
            }
        };

        RouteConfig routeConfig = new RouteConfig();
        Map<String, RouteInfo> concreteUrlRouteInfoMap = ConcreteUrlRouteInfoParser.parse(routeConfig);

        assertThat(concreteUrlRouteInfoMap, is(anEmptyMap()));
    }

    @Test
    public void testHasConfig() {

        SubUrlMethodMapping subUrlMethodMapping1 = new SubUrlMethodMapping();
        subUrlMethodMapping1.setSubUrl("/path1");
        subUrlMethodMapping1.setMethod("echo");

        SubUrlMethodMapping subUrlMethodMapping2 = new SubUrlMethodMapping();
        subUrlMethodMapping2.setSubUrl("/path2");
        subUrlMethodMapping2.setMethod("hello");

        List<SubUrlMethodMapping> subUrlMethodMappings = new ArrayList<>();
        subUrlMethodMappings.add(subUrlMethodMapping1);
        subUrlMethodMappings.add(subUrlMethodMapping2);

        String serviceName = "com.guosen.zebra.example.FirstService";
        String urlPrefix = "/firstService";

        RouteConfig routeConfig = new RouteConfig();
        routeConfig.setServiceName(serviceName);
        routeConfig.setUrlPrefix(urlPrefix);
        routeConfig.setVersion("1.0.0");

        new Expectations() {
            {
                SubUrlMethodMappingParser.parse(anyString);
                result = subUrlMethodMappings;
            }
        };

        Map<String, RouteInfo> concreteUrlRouteInfoMap = ConcreteUrlRouteInfoParser.parse(routeConfig);

        assertThat(concreteUrlRouteInfoMap, is(not(anEmptyMap())));

        String echoUrl = "/firstService/path1";
        RouteInfo echoRouteInfo = concreteUrlRouteInfoMap.get(echoUrl);

        assertThat(echoRouteInfo, is(notNullValue()));
        assertThat(echoRouteInfo.getServiceName(), is(serviceName));
        assertThat(echoRouteInfo.getUrlPrefix(), is(urlPrefix));
        assertThat(echoRouteInfo.getMethod(), is("echo"));
        assertThat(echoRouteInfo.getVersion(), is("1.0.0"));


        String helloUrl = "/firstService/path2";
        RouteInfo helloRouteInfo = concreteUrlRouteInfoMap.get(helloUrl);

        assertThat(helloRouteInfo, is(notNullValue()));
        assertThat(helloRouteInfo.getServiceName(), is(serviceName));
        assertThat(helloRouteInfo.getUrlPrefix(), is(urlPrefix));
        assertThat(helloRouteInfo.getMethod(), is("hello"));
        assertThat(helloRouteInfo.getVersion(), is("1.0.0"));
    }
}
