package com.guosen.zebra.gateway.route.cache;

import com.guosen.zebra.gateway.dao.RouteConfigDao;
import com.guosen.zebra.gateway.route.model.RouteConfig;
import com.guosen.zebra.gateway.route.model.RouteInfo;
import com.guosen.zebra.gateway.route.parser.ConcreteUrlRouteInfoParser;
import com.guosen.zebra.gateway.route.parser.UrlPrefixRouteInfoParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;

/**
 * 路由信息初始化器
 */
@Component
public class RouterCacheInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(RouterCacheInitializer.class);

    @Autowired
    private RouteConfigDao routeConfigDao;

    @PostConstruct
    public void init() {
        LOGGER.info("Begin to init zebra API gateway route info.");

        List<RouteConfig> configs = routeConfigDao.getConfigs();
        if (CollectionUtils.isEmpty(configs)) {
            LOGGER.info("There is no route configurations.");
            return;
        }

        RouteConfigCache.getInstance().setOldConfigs(configs);

        for (RouteConfig routeConfig : configs) {
            String microServiceName = routeConfig.getServiceName();
            Map<String, RouteInfo> urlRouteInfoMapOfMicroService = ConcreteUrlRouteInfoParser.parse(routeConfig);
            Map<String, RouteInfo> urlPrefixRouteInfoMapOfMicroservice = UrlPrefixRouteInfoParser.parse(routeConfig);

            ConcreteUrlRouteCache.getInstance().update(microServiceName, urlRouteInfoMapOfMicroService);
            UrlPrefixRouteCache.getInstance().update(microServiceName, urlPrefixRouteInfoMapOfMicroservice);
        }

        LOGGER.info("Finish to init zebra API gateway route info.");
    }
}
